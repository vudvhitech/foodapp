//
//  LoginViewController.swift
//  FoodApp
//
//  Created by Vu, Dinh Van  on 12/8/20.
//

import UIKit

class LoginViewController: UIViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    
    @IBOutlet weak var containerView: UIView!
    
    var pageViewController: UIPageViewController
    var pageTitles: [String]
    var pageImages: [String]
    @IBOutlet weak var pageIndicator: UIPageControl!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    
    required init(coder aDecoder: NSCoder) {
        self.pageTitles = ["Over 200 Tips and Trick", "Discover Hidden Features", "Bookmark Favorite Tip", "Free Regular Update"]
        self.pageImages = ["login_bg.png", "login_bg.png", "login_bg.png", "login_bg.png"]
        pageViewController = UIPageViewController()
        super.init(coder: aDecoder)!
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pageIndicator.numberOfPages = pageImages.count
        pageIndicator.currentPage = 0
        self.pageViewController = self.storyboard!.instantiateViewController(withIdentifier: "PageViewController") as! UIPageViewController
        self.pageViewController.dataSource = self
        self.pageViewController.delegate = self
        
        if let startingViewController = self.viewControllerAtIndex(index: 0) {
            let viewControllers = [startingViewController]
            self.pageViewController.setViewControllers(viewControllers, direction: .forward, animated: false, completion: nil)
        }
        self.pageViewController.view.frame = CGRect(x: 0, y: 0, width: self.containerView.frame.size.width, height: self.containerView.frame.size.height)
        
        self.addChild(self.pageViewController)
        self.containerView.addSubview(self.pageViewController.view)
        self.pageViewController.didMove(toParent: self)
        txtEmail.layer.masksToBounds = true
        txtEmail.layer.cornerRadius = 24
        txtPassword.layer.masksToBounds = true
        txtPassword.layer.cornerRadius = 24
    }
    
    func viewControllerAtIndex(index: Int) -> PageContentViewController? {
        if self.pageTitles.count == 0 || index >= self.pageTitles.count {
            return nil
        }
        
        let pageContentViewController: PageContentViewController = self.storyboard!.instantiateViewController(withIdentifier: "PageContentController") as! PageContentViewController
        pageContentViewController.imageFile = self.pageImages[index]
        pageContentViewController.titleText = self.pageTitles[index]
        pageContentViewController.pageIndex = index
        return pageContentViewController
    }
    
    @IBAction func login(sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let tabbarController = storyboard.instantiateViewController(identifier: "TabbarController") as? TabbarController else {
            return
        }
        UIApplication.shared.windows.first?.rootViewController = tabbarController
        UIApplication.shared.windows.first?.makeKeyAndVisible()
    }
    
    
    // MARK: - PageViewControllerDataSource
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        var index = (viewController as! PageContentViewController).pageIndex
        
        if index == 0 || index == NSNotFound {
            return nil
        }
        
        index -= 1
        return viewControllerAtIndex(index: index)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        var index = (viewController as! PageContentViewController).pageIndex
        
        if index == NSNotFound {
            return nil
        }
        
        index += 1
        
        if index == self.pageTitles.count {
            return nil
        }
        return viewControllerAtIndex(index: index)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if (!completed)
        {
            return
        }
        if let controller = pageViewController.viewControllers!.first as? PageContentViewController {
            self.pageIndicator.currentPage = controller.pageIndex
        }
    }
    
    
    func presentationCountForPageViewController(pageViewController: UIPageViewController) -> Int {
        return self.pageTitles.count
    }
    
    func presentationIndexForPageViewController(pageViewController: UIPageViewController) -> Int {
        return 0
    }
    
}
