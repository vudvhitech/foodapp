//
//  Food.swift
//  FoodApp
//
//  Created by Vu, Dinh Van  on 12/11/20.
//

import Foundation

class Food {
    let image: String
    let name: String
    let place: String
    
    init(image: String, name: String, place: String) {
        self.image = image
        self.name = name
        self.place = place
    }
}
