//
//  Drink.swift
//  FoodApp
//
//  Created by Vu, Dinh Van  on 12/11/20.
//

import Foundation

class Drink {
    let image: String
    let name: String
    let address: String
    let rate: String
    let rating: String
    
    init(image: String, name: String, address: String, rate: String, rating: String) {
        self.image = image
        self.name = name
        self.address = address
        self.rate = rate
        self.rating = rating
    }
}
