//
//  LoginBackgroundView.swift
//  FoodApp
//
//  Created by Vu, Dinh Van  on 12/8/20.
//

import UIKit

class LoginBackgroundView: UIView {

    override func layoutSubviews() {
        super.layoutSubviews()
        self.roundCorners(corners: [.topLeft, .topRight], radius: 16)
    }

}
