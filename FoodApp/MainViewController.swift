//
//  ViewController.swift
//  FoodApp
//
//  Created by Vu, Dinh Van  on 12/8/20.
//

import UIKit
import LNPopupController

class MainViewController: UIViewController {
    
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var filterButton: UIButton!
    @IBOutlet weak var foodCollection: UICollectionView!
    @IBOutlet weak var drinkCollection: UICollectionView!
    
    let foods = [
        Food(image: "pizza", name: "Pizza", place: "2350 places"),
        Food(image: "hamburger", name: "Burgers", place: "350 places"),
        Food(image: "meat", name: "Steak", place: "834 places"),
        Food(image: "spaguetti", name: "Pasta", place: "150 places"),
    ]
    
    let drinks = [
        Drink(image: "alireza-etemadi", name: "Rahat Brasserie", address: "124 Levent Besiktas", rate: "4.9", rating: "120 ratings"),
        Drink(image: "brooke-lark", name: "Garage Bar", address: "124 Levent Besiktas", rate: "4.9", rating: "120 ratings")
    ]

    override func viewDidLoad() {
        super.viewDidLoad()
        txtSearch.layer.masksToBounds = true
        txtSearch.layer.cornerRadius = 26
        txtSearch.layer.borderWidth = 1
        txtSearch.layer.borderColor = UIColor.gray.cgColor
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 30, height: 48))
        txtSearch.leftViewMode = .always
        txtSearch.leftView = view
    }


    @IBAction func filter(sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let filterViewController = storyboard.instantiateViewController(identifier: "FilterViewController") as? FilterViewController else {
            return
        }
            
        tabBarController?.presentPopupBar(withContentViewController: filterViewController, animated: true, completion: nil)
    }
    
}


extension MainViewController: UICollectionViewDelegate {
    
}

extension MainViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == foodCollection {
            let food = foods[indexPath.row]
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "foodCell", for: indexPath) as? FoodCell else {
                return UICollectionViewCell()
            }
            cell.item = food
            return cell
        } else {
            let drink = drinks[indexPath.row]
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "drinkCell", for: indexPath) as? DrinkCell else {
                return UICollectionViewCell()
            }
            cell.item = drink
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == foodCollection {
            return foods.count
        } else {
            return drinks.count
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
}
