//
//  RegisterViewController.swift
//  FoodApp
//
//  Created by Vu, Dinh Van  on 12/8/20.
//

import UIKit

class RegisterViewController: UIViewController {
    
    @IBOutlet weak var txtPhone: UITextField!
    @IBOutlet weak var bottomRegisterConstant: NSLayoutConstraint!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        txtPhone.layer.masksToBounds = true
        txtPhone.layer.cornerRadius = 24
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 80, height: 48))
        txtPhone.leftViewMode = .always
        txtPhone.leftView = view
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillShow),
            name: UIResponder.keyboardWillShowNotification,
            object: nil
        )
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillHidden),
            name: UIResponder.keyboardWillHideNotification,
            object: nil
        )
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            bottomRegisterConstant.constant = keyboardHeight + 20
        }
    }
    
    
    @objc func keyboardWillHidden(_ notification: Notification) {
        bottomRegisterConstant.constant = 36
    }
}
