//
//  DrinkCell.swift
//  FoodApp
//
//  Created by Vu, Dinh Van  on 12/11/20.
//

import UIKit

class FoodCell: UICollectionViewCell {
    @IBOutlet weak var drinkView: UIView!
    @IBOutlet weak var drinkImageView: UIImageView!
    @IBOutlet weak var drinkNameLabel: UILabel!
    @IBOutlet weak var placeLabel: UILabel!
    
    var item: Food? {
        didSet {
            if let item = item {
                drinkImageView.image = UIImage(named: item.image)
                dropShadow()
                drinkNameLabel.text = item.name
                placeLabel.text = item.place
            }
        }
    }
    
    override class func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func dropShadow() {
        drinkView.layer.shadowColor = UIColor.gray.cgColor
        drinkView.layer.shadowOpacity = 0.3
        drinkView.layer.shadowOffset = CGSize.zero
        drinkView.layer.shadowRadius = 5
      }
}
