//
//  DrinkCell.swift
//  FoodApp
//
//  Created by Vu, Dinh Van  on 12/11/20.
//

import UIKit

class DrinkCell: UICollectionViewCell {
    @IBOutlet weak var foodImageView: UIImageView!
    @IBOutlet weak var foodNameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var rateLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var deliveryButton: UIButton!
    
    var item: Drink? {
        didSet {
            if let item = item {
                foodImageView.image = UIImage(named: item.image)
                foodNameLabel.text = item.name
                ratingLabel.text = item.rating
                rateLabel.text = item.rate
                addressLabel.text = item.address
            }
        }
    }
    
}
